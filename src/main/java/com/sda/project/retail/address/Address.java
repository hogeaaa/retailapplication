package com.sda.project.retail.address;

import java.util.Objects;

public class Address {
    private int number;
    private String street;
    private String county;

    public Address(){}

    public Address(int number, String street, String county) {
        this.number = number;
        this.street = street;
        this.county = county;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    @Override
    public String toString() {
        return "Address{" +
                "number=" + number +
                ", street='" + street + '\'' +
                ", county='" + county + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return number == address.number &&
                Objects.equals(street, address.street) &&
                Objects.equals(county, address.county);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, street, county);
    }
}
