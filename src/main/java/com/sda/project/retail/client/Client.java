package com.sda.project.retail.client;

import com.sda.project.retail.address.Address;

public class Client {
    private int clientId;
    private String name;
    private String CNP;
    private int age;
    private String phoneNumber;
    private Address address;

    public Client(int clientId, String name, String CNP, int age, String phoneNumber, Address address) {
        this.clientId = clientId;
        this.name = name;
        this.CNP = CNP;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }
    public Client(){
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", name='" + name + '\'' +
                ", CNP='" + CNP + '\'' +
                ", age=" + age +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address=" + address +
                '}';
    }
}
