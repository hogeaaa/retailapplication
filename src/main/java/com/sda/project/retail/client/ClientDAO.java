package com.sda.project.retail.client;

import com.sda.project.retail.utils.GenericCRUD;

import java.util.ArrayList;
import java.util.List;

public class ClientDAO extends GenericCRUD<Client> {

    private List<Client> clients;

    public static ClientDAO instance;

    public ClientDAO() {
        clients = new ArrayList<>();
    }

    public static ClientDAO getInstance(){
        if(instance == null){
            instance = new ClientDAO();
        }
        return instance;
    }



    @Override
    public void insert(Client element) {
        clients.add(element);
    }

    @Override
    public Client get(int id) {
        for (Client client : clients) {
            if (id == client.getClientId()) {
                return client;
            }
        }
        return null;
    }

    @Override
    public List<Client> getAll() {
        return clients;
    }

    @Override
    public void delete(int id) {
        for (Client client : clients) {
            if (id == client.getClientId()) {
                this.clients.remove(client);
                break;
            }
        }
    }

    @Override
    public void deleteAll() {
        clients.clear();
        // clients = new ArrayList<>(); - > reinitializam lista cu o lista noua
    }

    @Override
    public void update(Client element) {
        Client actualClient = null;
        for (Client client : clients) {
            if (element.getClientId() == client.getClientId()) {
                //actualClient = client;
                client.setAddress(element.getAddress());
                client.setAge(element.getAge());
                client.setCNP(element.getCNP());
                client.setName(element.getName());
                client.setPhoneNumber(element.getPhoneNumber());
            }
        }

    }
}
