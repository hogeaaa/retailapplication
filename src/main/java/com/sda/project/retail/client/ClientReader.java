package com.sda.project.retail.client;

import com.sda.project.retail.address.Address;
import com.sda.project.retail.display.Display;
import com.sda.project.retail.utils.Consts;

import javax.swing.text.AbstractDocument;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ClientReader {

    public List<Client> readClientsFromFile(String filepath){
        List<Client> clients = new LinkedList<>();
       // File inputFile = new File(filepath); -> does nothing
        try {
            FileReader reader = new FileReader(filepath);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();

            while(line!=null){

                String[] elements = line.split(" ");
                Address address = new Address();
                Client client = new Client();
                client.setClientId(Integer.valueOf(elements[0]));
                client.setName(elements[1]);
                client.setCNP(elements[2]);
                client.setAge(Integer.valueOf(elements[3]));
                client.setPhoneNumber(elements[4]);
                address.setNumber(Integer.valueOf(elements[5]));
                address.setStreet(elements[6]);
                address.setCounty(elements[7]);

                client.setAddress(address);
                clients.add(client);

                line = bufferedReader.readLine();

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

        return clients;
    }

    public Client readClientFromKeyboard(Scanner scanner){
        Client client = new Client();
        Address address = new Address();

        Display.displayMessage(Consts.INSERT_CLIENT_ID);
        int clientId = scanner.nextInt();
        client.setClientId(clientId);

        Display.displayMessage(Consts.INSERT_CLIENT_NAME);
        String name = scanner.next();
        client.setName(name);

        Display.displayMessage(Consts.INSERT_CLIENT_AGE);
        int age = scanner.nextInt();
        client.setAge(age);

        Display.displayMessage(Consts.INSERT_CLIENT_CNP);
        String CNP = scanner.next();
        client.setCNP(CNP);

        Display.displayMessage(Consts.INSERT_CLIENT_PHONE_NUMBER);
        String phoneNumber = scanner.next();
        client.setPhoneNumber(phoneNumber);

        Display.displayMessage(Consts.INSERT_ADDRESS_NUMBER);
        int addressNumber = scanner.nextInt();
        address.setNumber(addressNumber);

        Display.displayMessage(Consts.INSERT_ADDRESS_STREET);
        String street = scanner.next();
        address.setStreet(street);

        Display.displayMessage(Consts.INSERT_ADDRESS_COUNTY);
        String couty = scanner.next();
        address.setCounty(couty);

        client.setAddress(address);

        return client;
    }

}
