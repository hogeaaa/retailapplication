package com.sda.project.retail.client;

import com.sda.project.retail.product.ProductService;
import com.sda.project.retail.utils.Consts;
import org.apache.log4j.Logger;

import java.util.List;

public class ClientService implements ClientServiceInterface {

    private ClientDAO clientDAO;

    private ClientReader clientReader;

    final static Logger logger = Logger.getLogger(ProductService.class);

    public ClientReader getClientReader() {
        return clientReader;
    }

    public ClientService() {
        clientDAO = ClientDAO.getInstance();
        clientReader = new ClientReader();
        clientInitialization(clientReader.readClientsFromFile(Consts.PATH_TO_CLIENT_FILE));
    }

    public void clientInitialization(List<Client> clients) {
        for (Client client : clients) {
            clientDAO.insert(client);
        }
    }

    public void insertClient(Client client) {
        clientDAO.insert(client);
    }

    @Override
    public Client getClient(int id) {
       return clientDAO.get(id);
    }

    @Override
    public List<Client> getAllClient() {
        return clientDAO.getAll();
    }

    @Override
    public void deleteClient(int id) {
        clientDAO.delete(id);
    }

    @Override
    public void deleteAllClients() {
        clientDAO.deleteAll();
    }

    @Override
    public void updateClient(Client client) {
        clientDAO.update(client);
    }


}
