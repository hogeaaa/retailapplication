package com.sda.project.retail.client;

import java.util.List;

public interface ClientServiceInterface {

    public void insertClient(Client client);

    public Client getClient(int id);

    public List<Client> getAllClient();

    public void deleteClient(int id);

    public void deleteAllClients();

    public void updateClient(Client client);

}
