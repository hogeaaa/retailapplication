package com.sda.project.retail.display;

import com.sda.project.retail.client.Client;
import com.sda.project.retail.product.Product;

import java.util.List;

public class Display {

    public static void displayMessage(String message){
        System.out.println(message);
    }

    public static void newLine(){
        System.out.println();
    }

    public static void displayClient(Client client){
        System.out.println(client);
    }

    public static void displayAllClients(List<Client> clients){
        for(Client client: clients){
            displayClient(client);
        }
        Display.newLine();
    }

    public static void displayProduct(Product product){
        System.out.println(product);
    }

    public  static void displayAllProducts(List<Product> products){
        for(Product product : products){
            displayProduct(product);
        }
        Display.newLine();
    }

}
