package com.sda.project.retail.logic;

import com.sda.project.retail.client.Client;
import com.sda.project.retail.client.ClientService;
import com.sda.project.retail.display.Display;
import com.sda.project.retail.product.Product;
import com.sda.project.retail.product.ProductService;
import com.sda.project.retail.utils.Consts;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import java.util.List;
import java.util.Scanner;

public class ApplicationLogic {

    private ClientService clientService;
    private ProductService productService;



    public ApplicationLogic(){
        clientService = new ClientService();
        productService = new ProductService();
    }

    public void runLogic(){

        Scanner scanner = new Scanner(System.in);

        boolean isRunning = true;
        Display.displayMessage(Consts.WELCOME_MESSAGE);

        while(isRunning){
            Display.displayMessage(Consts.DISPLAY_ALL_CLIENTS);
            Display.displayMessage(Consts.DISPLAY_ALL_PRODUCTS);
            Display.displayMessage(Consts.INSERT_NEW_CLIENT);
            Display.displayMessage(Consts.INSERT_NEW_PRODUCT);
            Display.displayMessage(Consts.EXIT_APPLICATION);
            Display.newLine();
            Display.displayMessage(Consts.INSERT_OPTION);

            int option = scanner.nextInt();

            switch (option){
                case 1:
                    List<Client> clients = clientService.getAllClient();
                    Display.displayAllClients(clients);
                    break;
                case 2:
                    List<Product> products = productService.getAllProduct();
                    Display.displayAllProducts(products);
                    break;
                case 3:
                    Client client = clientService.getClientReader().readClientFromKeyboard(scanner);
                    clientService.insertClient(client);
                    break;
                case 4:
                    Product product = productService.getProductReader().readProductFromKeyboard(scanner);
                    productService.insertProduct(product);
                    break;
                case 5:
                    isRunning = false;
                    break;
                default:
                    Display.displayMessage(Consts.INSERT_VALID_OPTION);
            }

        }
        scanner.close();
    }
}
