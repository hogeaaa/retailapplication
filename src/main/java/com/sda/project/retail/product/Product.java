package com.sda.project.retail.product;

public class Product {

    private int productId;
    private int price;
    private String color;
    private String type;
    private String barcode;

    public Product(){}

    public Product(int productId, int price, String color, String type, String barcode) {
        this.productId = productId;
        this.price = price;
        this.color = color;
        this.type = type;
        this.barcode = barcode;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", price=" + price +
                ", color='" + color + '\'' +
                ", type='" + type + '\'' +
                ", barcode='" + barcode + '\'' +
                '}';
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
