package com.sda.project.retail.product;

import com.sda.project.retail.client.ClientDAO;
import com.sda.project.retail.utils.GenericCRUD;

import java.util.ArrayList;
import java.util.List;

public class ProductDAO extends GenericCRUD<Product> {

    public List<Product> products;
    public static ProductDAO instance;

    public ProductDAO(){
        products = new ArrayList<>();
    }

    public static ProductDAO getInstance(){
        if(instance == null){
            instance = new ProductDAO();
        }
        return instance;
    }

    @Override
    public void insert(Product element) {
        this.products.add(element);
    }

    @Override
    public Product get(int id) {
        for(Product product : products){
            if(id==product.getProductId()){
                return product;
            }
        }
        return null;
    }

    @Override
    public List<Product> getAll() {
        return products;
    }

    @Override
    public void delete(int id) {
        for(Product product : products){
            if(id == product.getProductId()){
                this.products.remove(product);
            }
        }
    }

    @Override
    public void deleteAll() {
        products.clear();
    }

    @Override
    public void update(Product element) {
        for(Product product : products){
            if(element.getProductId()==product.getProductId()){
                product.setBarcode(element.getBarcode());
                product.setColor(element.getColor());
                product.setPrice(element.getPrice());
                product.setProductId(element.getProductId());
                product.setType(element.getType());
            }
        }
    }
}
