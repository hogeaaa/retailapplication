package com.sda.project.retail.product;

import com.sda.project.retail.display.Display;
import com.sda.project.retail.utils.Consts;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ProductReader {
    final static Logger logger = Logger.getLogger(ProductService.class);
    public List<Product> readProductsFromFile(String filepath) {
        List<Product> products = new LinkedList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filepath));
            String line = reader.readLine();
            while (line != null) {
                String[] elements = line.split(" ");
                Product product = new Product();
                product.setProductId(Integer.valueOf(elements[0]));
                product.setPrice(Integer.valueOf(elements[1]));
                product.setColor(elements[2]);
                product.setType(elements[3]);
                product.setBarcode(elements[4]);

                products.add(product);
                line = reader.readLine();

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            logger.error(e);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e);
            logger.info(e);
            logger.warn(e);
        }
        return products;
    }
    public Product readProductFromKeyboard(Scanner scanner){
        Product product = new Product();

        Display.displayMessage(Consts.INSERT_PRODUCT_ID);
        int clientID = scanner.nextInt();
        product.setProductId(clientID);

        Display.displayMessage(Consts.INSERT_PRODUCT_PRICE);
        int price = scanner.nextInt();
        product.setPrice(price);

        Display.displayMessage(Consts.INSERT_PRODUCT_COLOR);
        String color = scanner.next();
        product.setColor(color);

        Display.displayMessage(Consts.INSERT_PRODUCT_TYPE);
        String type = scanner.next();
        product.setType(type);

        Display.displayMessage(Consts.INSERT_PRODUCT_BARCODE);
        String barecode = scanner.next();
        product.setBarcode(barecode);

        return product;
    }
}
