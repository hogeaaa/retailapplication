package com.sda.project.retail.product;

import com.sda.project.retail.client.Client;
import com.sda.project.retail.client.ClientDAO;
import com.sda.project.retail.client.ClientReader;
import com.sda.project.retail.utils.Consts;
import org.apache.log4j.Logger;

import java.util.List;

public class ProductService implements ProductServiceInterface {

    final static Logger logger = Logger.getLogger(ProductService.class);

    ProductDAO productDAO;

    private ProductReader productReader;

    public ProductReader getProductReader() {
        return productReader;
    }

    public ProductService() {
        productDAO = ProductDAO.getInstance();
        productReader = new ProductReader();
        productInitialization(productReader.readProductsFromFile(Consts.PATH_TO_PRODUCT_FILE));
    }

    public void productInitialization(List<Product> products) {
        logger.info("Entered in product initialization with elements: " + products);
        for (Product product : products) {
            productDAO.insert(product);
        }
    }

    @Override
    public void insertProduct(Product product) {
        productDAO.insert(product);
    }

    @Override
    public Product getProduct(int id) {
        return productDAO.get(id);
    }

    @Override
    public List<Product> getAllProduct() {
        return productDAO.getAll();
    }

    @Override
    public void deleteProduct(int id) {
        productDAO.delete(id);
    }

    @Override
    public void deleteAllProduct() {
        productDAO.deleteAll();
    }

    @Override
    public void updateProduct(Product product) {
        productDAO.update(product);
    }
}
