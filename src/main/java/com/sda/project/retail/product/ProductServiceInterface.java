package com.sda.project.retail.product;

import com.sda.project.retail.client.Client;

import java.util.List;

public interface ProductServiceInterface {

    public void insertProduct(Product product);

    public Product getProduct(int id);

    public List<Product> getAllProduct();

    public void deleteProduct(int id);

    public void deleteAllProduct();

    public void updateProduct(Product product);

}
