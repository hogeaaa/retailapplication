package com.sda.project.retail.runner;

import com.sda.project.retail.logic.ApplicationLogic;

public class ApplicationRunner {
    public static void main(String[] args) {
        ApplicationLogic logic = new ApplicationLogic();
        logic.runLogic();
    }
}
