package com.sda.project.retail.utils;

public class Consts {

    /*
    constante cai fisiere
     */

    public static final String PATH_TO_CLIENT_FILE = "C:\\Users\\andre\\IdeaProjects\\RetailApplication\\src\\main\\resources\\client\\client.txt";
    public static final String PATH_TO_PRODUCT_FILE = "C:\\Users\\andre\\IdeaProjects\\RetailApplication\\src\\main\\resources\\product\\product.txt";





    /*
    constante mesaje client
     */

    public static final String INSERT_CLIENT_ID = "Please insert a client id: ";
    public static final String INSERT_CLIENT_NAME = "Please insert a client name: ";
    public static final String INSERT_CLIENT_CNP = "Please insert a client CNP: ";
    public static final String INSERT_CLIENT_AGE = "Please insert a client age: ";
    public static final String INSERT_CLIENT_PHONE_NUMBER = "Please insert a client phone numer: ";

    /*
    constante mesaje adresa
     */

    public static final String INSERT_ADDRESS_NUMBER = "Please insert a address numer: ";
    public static final String INSERT_ADDRESS_STREET = "Please insert a address street: ";
    public static final String INSERT_ADDRESS_COUNTY = "Please insert a address county: ";

    /*
    constante mesaje product
     */
    public static final String INSERT_PRODUCT_ID = "Please insert a product ID: ";
    public static final String INSERT_PRODUCT_PRICE = "Please insert a product price: ";
    public static final String INSERT_PRODUCT_COLOR = "Please insert a product color: ";
    public static final String INSERT_PRODUCT_TYPE = "Please insert a product type: ";
    public static final String INSERT_PRODUCT_BARCODE = "Please insert a product barcode: ";
    /*
    General messages
     */
    public static final String WELCOME_MESSAGE = "Welcome to Emag!";
    public static final String DISPLAY_ALL_CLIENTS = "1.Display all clients!";
    public static final String DISPLAY_ALL_PRODUCTS = "2.Display all products!";
    public static final String INSERT_NEW_CLIENT = "3.Insert new client!";
    public static final String INSERT_NEW_PRODUCT = "4.Insert new product!";
    public static final String EXIT_APPLICATION = "5.Emag out bitcheez!";
    public static final String INSERT_OPTION = "Please insert an option: ";
    public static final String INSERT_VALID_OPTION = "Please insert a valid option: ";

}
