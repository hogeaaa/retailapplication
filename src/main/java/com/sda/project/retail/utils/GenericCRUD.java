package com.sda.project.retail.utils;

import java.util.List;

public abstract class GenericCRUD<T> {

    public abstract void insert(T element);

    public abstract T get (int id);

    public abstract List<T> getAll();

    public abstract void delete(int id);

    public abstract void deleteAll();

    public abstract void update(T element);

}
